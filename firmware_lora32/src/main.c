#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "esp_log.h"
#include "sdkconfig.h"

#define LED_PIN GPIO_NUM_25

static uint8_t led_state = 0;

static void configure_led(void)
{
    gpio_reset_pin(LED_PIN);
    gpio_set_direction(LED_PIN, GPIO_MODE_OUTPUT);
}

static void blink_led(void)
{
    gpio_set_level(LED_PIN, led_state);
}

void app_main() 
{
    configure_led();

    while (1)
    {
        blink_led();

        led_state = !led_state;
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}