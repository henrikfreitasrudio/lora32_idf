# lora32_idf



## Board info
- TTGO LoRa32 915MHz
- Model T3_V1.6.1(20210104)

## Dev IDE
- PlatformIO on VsCode

## Dev Framework
- ESP IDF v.5.1.2

## License
For open source projects, say how it is licensed.

